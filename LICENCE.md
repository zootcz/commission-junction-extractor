# Terms and Conditions

The CJAffiliate extractor is built and maintained by ZOOT a.s. Usage is free, but support may be charged. We are not collecting or tracking your data.

For infomartions feel free contact us at: [dwh@zoot.cz](mailto://dwh@zoot.cz)

## Contact address:

ZOOT a.s.  
Drtinova 557/10  
150 00 Praha 5-Smíchov  
email: [dwh@zoot.cz](mailto://dwh@zoot.cz)