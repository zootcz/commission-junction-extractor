# README

This repositary holds publicly accessible files describing KBC component "CJAffiliate extractor".
"CJAffiliate extractor" retrieve data from API: CJAffiliate -> Commission detail service API -> Commissions Resource Request method.

## Beware of this gotchas

* Output mapping is made as **full import** (not incremental). We will add incremental import as option in future.
* Commissions Resource Request method is called with **date-type=posting** E.g: url: https://commission-detail.api.cj.com/v3/commissions?date-type=posting&start-date=2017-06-20&end-date=2017-06-21

### Contact 

* ZOOT a.s.
* dwh@zoot.cz