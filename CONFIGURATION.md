#  Configuration

Please fill:

## Accounts

Definiton for at least one account whose data will be fetched.

**Name**
(optional) - Account name just for distinction between accounts. 
    
**Developer key**
(mandatory) - Developer key for this account. A developer key is a unique identification string assigned to you once you register for CJ's Web Services. The required developer key provides security and authentication when accessing data and the CJ platform using the Web Services APIs.

**Destination table**
(mandatory) - Name of table where fetched data will be saved.
**Beware - output mapping is made as full import (not incremental).**
We will add incremental import as option in future.

E.g.: out.c-data.my-table (Save to my-table table in the out-c-data bucket)

## Count days to history 
(mandatory)

Length of fetched history in days. E.g.: 28
